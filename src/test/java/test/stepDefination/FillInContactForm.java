package test.stepDefination;


import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import com.automationpractice.core.base.Globals;
import com.automationpractice.core.pages.ContactForm;
import com.automationpractice.core.pages.HomePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class FillInContactForm {


	@Then("^I click on Contact button on HomePage and navigate to contact form$")
	public void i_click_on_contact_button() throws Throwable {
		new HomePage().contactformnavigation();
		new ContactForm().contactform();
	}


	@Then("I enter all the required details such as {string} {string} {string} and {string}")
	public void i_enter_all_the_required_details_such_as_and(String string, String string2, String string3, String string4) {
		new ContactForm().enterdetails(string, string2, string3, string4);
	}

	@Then("I click on the question\\/comment section and i enter my {string}")
	public void i_click_on_the_question_comment_section_and_i_enter_my(String string) {
		new ContactForm().entercommentwithnoreservation(string);
	}

	@And("^I Take a Screenshot$")
	public void i_take_screenshot() throws IOException {
		File src= ((TakesScreenshot)Globals.getDriver()).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File(System.getProperty("user.dir") + "\\src\\test\\resources\\screenshots\\ScreenShot.png"));
	}
}

