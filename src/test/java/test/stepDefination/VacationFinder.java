package test.stepDefination;

import java.util.HashMap;
import com.automationpractice.core.base.Globals;
import com.automationpractice.core.pages.HomePage;
import com.automationpractice.core.utils.ConfigFileReader;
import com.automationpractice.core.utils.ExcelUtils;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class VacationFinder {

	@Given("^I am on Home Page$")
	public void i_search_an_item_on_HomePage() throws Throwable {
		HashMap<String, Object> testData = new ExcelUtils().getTestDataset("RegisterUser");
		Globals.getDriver().get(new ConfigFileReader().getProperty("testurl"));
	}

	@Then("I select {string} as country")
	public void i_select_as_country(String string) {
		new HomePage().selectCountry(string);
	}

	@Then("I select Nederland as desired country and Drenthe as desired region to visit from dropdown option")
	public void i_select_as_desired_country_to_visit_from_dropdown_option() {
		new HomePage().vacationcountryfinder();
	}

	@Then("I select {string} to {string} as vacation dates")
	public void i_select_to_as_vacation_dates(String string, String string2) {
		// Write code here that turns the phrase above into concrete actions
		
	}

	@And("I select number of person i want to go on vacation")
	public void i_select_as_number_of_person_i_want_to_go_on_vacation() {
		new HomePage().vacationwithwhom();
		
	}

	@Then("I select ALL as Accomodation Type")
	public void i_select_as_Accomodation_Type() {
		new HomePage().vacationaccomodation();
	}
	
	@Then("Search for desired Camp option")
	public void search_camp() {
		new HomePage().searchcamp();

	}
	
}
