package com.automationpractice.core.base;


import java.io.*;
import java.util.*;


public class test {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter wr = new PrintWriter(System.out);
		int N = Integer.parseInt(br.readLine().trim());
		String[] data = new String[N];
		for (int i_data = 0; i_data < N; i_data++) {
			data[i_data] = br.readLine();
		}

		String[] out_ = database_clean(data);
		for (int i_out_ = 0; i_out_ < out_.length; i_out_++) {
			
			Department department = getReference(out_[i_out_].split(" ")[0]);
			department.setName(out_[i_out_].split(" ")[1]);
			department.display();
			//wr.println(out_[i_out_]);
		}

		wr.close();
		br.close();
	}

	static String[] database_clean(String[] data) {
		String[] cleanedData = new String[data.length];
				for (int i_out_ = 0; i_out_ < data.length; i_out_++) {
					cleanedData[i_out_] = removeSpecialChar(data[i_out_]);
				}

		return cleanedData;
	}
	public static Department getReference(String name){
		
		Department department = null;
		
		if(name.equalsIgnoreCase("Sales")){
			department =   new Sales();
		}
		else if(name.equalsIgnoreCase("ProblemSetter")){
			department = new ProblemSetter();
		}
		else if(name.equalsIgnoreCase("Developer")){
			department = new Developer();
		}
		return department;
	}
	public static String removeSpecialChar(String inp){
		return inp.replaceAll("[$#&]","");

	}
}

interface Department{

	public void setName(String name);
	public void display();
}

class ProblemSetter implements Department{
	String name;

	public void setName(String name){
		this.name = name;
	}

	public void display(){
		System.out.println("Problem Setter");
		System.out.println("Name: " + name);
	}
}

class Developer implements Department{
	String name;

	public void setName(String name){
		this.name = name;
	}

	public void display(){
		System.out.println("Developer");
		System.out.println("Name: " + name);
	}

}
class Sales implements Department{
	String name;

	public void setName(String name){
		this.name = name;
	}

	public void display(){
		System.out.println("Sales");
		System.out.println("Name: " + name);
	}

}