package com.automationpractice.core.pages;



import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.automationpractice.core.base.ElementActions;
import com.automationpractice.core.base.Globals;

public class HomePage {
	
	private ElementActions elementActions = new ElementActions();
	
	@FindBy(xpath = "(//span[contains(.,'Contact')])[2]")
	private WebElement HPContactButton;
	
	@FindBy(xpath = "(//div[contains(@class,'landenimg nl')])[1]")
	private WebElement NLCountry;
	
	@FindBy(xpath = "//a[contains(.,'Ik ga akkoord')]")
	private WebElement cookies;
	
	@FindBy(xpath = "(//button[@type='button'])[1]")
	private WebElement CountryDropdown;
	
	@FindBy(xpath = "(//button[@type='button'])[2]")
	private WebElement RegionDropdown;
	
	@FindBy(xpath = "//span[contains(.,'Frankrijk')]")
	private WebElement FRvacationcountry;
	
	@FindBy(xpath = "//a[contains(.,'Camping Iris Parc Le Château de Galaure')]")
	private WebElement CampingIrisParcLeChâteaudeGalaure;
	
	@FindBy(xpath = "//span[contains(.,'Drenthe')]")
	private WebElement DRvacationregion;
	
	@FindBy(xpath = "//span[contains(.,'2 personen')]")
	private WebElement WithWhomDropDown;
	
	@FindBy(xpath = "//span[contains(.,'Bevestig reisgezelschap')]")
	private WebElement WithWhomConfirm;
	
	@FindBy(xpath = "//button[contains(.,'5 types')]")
	private WebElement AccomodationDropDown;
	
	@FindBy(xpath = "//span[contains(.,'Bevestig uw keuze')]")
	private WebElement AccomodationConfirm;
	
	@FindBy(xpath = "//strong[contains(.,'juli 2019')]")
	private WebElement CurrentYear;
	
	@FindBy(xpath = "//span[contains(.,'Zoek uw vakantie')]")
	private WebElement SearchVacation;
	
	public HomePage(){
		
		PageFactory.initElements(Globals.getDriver(), this);
	}
	

	public void selectCountry(String country) {
		elementActions.ElementClick(NLCountry);
		elementActions.ElementClick(cookies);
	}
	
	public void contactformnavigation() {
		elementActions.scrollToElement(HPContactButton);
		elementActions.ElementClick(HPContactButton);
		}
	
	public void vacationcountryfinder() {
		elementActions.ElementClick(CountryDropdown);
		elementActions.ElementClick(FRvacationcountry);
		elementActions.ElementClick(RegionDropdown);
		//elementActions.ElementClick(DRvacationregion);
	}
	
	
	public void vacationwithwhom() {
		elementActions.ElementClick(WithWhomDropDown);
		elementActions.ElementClick(WithWhomConfirm);
	}
	
	public void vacationaccomodation() {
		elementActions.ElementClick(AccomodationDropDown);
		elementActions.ElementClick(AccomodationConfirm);
		elementActions.ElementClick(SearchVacation);
	}
	
	public void searchcamp() {
		if(elementActions.isElementDisplay(CampingIrisParcLeChâteaudeGalaure, 15)) {
		System.out.println(" Yes , CampingIrisParcLeChâteaudeGalaure is in your filtered vacation list");}
		else {
			System.out.println(" Sorry, CampingIrisParcLeChâteaudeGalaure is not present in your filtered vacation List");
		}
			
		
	}
}
