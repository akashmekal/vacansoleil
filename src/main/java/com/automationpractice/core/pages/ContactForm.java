package com.automationpractice.core.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.automationpractice.core.base.ElementActions;
import com.automationpractice.core.base.Globals;

public class ContactForm {

	
	private ElementActions elementActions = new ElementActions();
	
	@FindBy(xpath = "(//a[@class=' '])[2]")
	private WebElement ContactForm;
	
	@FindBy(xpath = "//input[contains(@data-exampletext,'Voornaam *')]")
	private WebElement FName;
	
	@FindBy(xpath = "//input[contains(@data-exampletext,'Achternaam *')]")
	private WebElement LName;
	
	@FindBy(xpath = "//input[contains(@id,'phone')]")
	private WebElement Telphn;
	
	@FindBy(xpath = "//input[@type='email']")
	private WebElement Email;
	
	@FindBy(xpath = "(//input[@type='radio'])[1]")
	private WebElement CommentRadioButtonwithNoReservation;
	
	@FindBy(xpath = "//textarea[@id='id_question']")
	private WebElement CommentBody;
	
	
	public ContactForm(){
		
		PageFactory.initElements(Globals.getDriver(), this);
	}
	
	
	public void contactform() {
	elementActions.ElementClick(ContactForm);
	}
	
	public void enterdetails(String FirstName, String LastName, String Telephone, String EmailID) {
		elementActions.ElementClick(FName);
		elementActions.TypeElement(FName, FirstName);
		elementActions.ElementClick(LName);
		elementActions.TypeElement(LName, LastName);
		elementActions.ElementClick(Telphn);
		elementActions.TypeElement(Telphn, Telephone);
		elementActions.ElementClick(Email);
		elementActions.TypeElement(Email, EmailID);
	}
	
	public void entercommentwithnoreservation(String comment) {
		elementActions.ElementClick(CommentRadioButtonwithNoReservation);
		elementActions.ElementClick(CommentBody);
		elementActions.TypeElement(CommentBody, comment);
		
	}
	
	
}
